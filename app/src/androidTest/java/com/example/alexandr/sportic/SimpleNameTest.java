package com.example.alexandr.sportic;

/**
 * Created by Alexandr on 17.05.2016.
 */
import android.test.ActivityInstrumentationTestCase2;



public class SimpleNameTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public SimpleNameTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testEnterName() throws Exception {
        onView(withId(R.id.category)).perform(typeText("Barsik"));
        onView(withId(R.id.list)).perform(click());
        onView(withId(R.id.description)).check(matches(withText("Hello Barsik")));
    }
}
package com.example.alexandr.sportic;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.quickblox.customobjects.model.QBCustomObject;

/**
 * Created by Alexandr on 17.05.2016.
 */


public class FeedReaderDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "SPORT"; // Имя базы данных
    private static final int DB_VERSION = 2; // Версия базы данных
    FeedReaderDbHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        updateMyDatabase(db, 0, DB_VERSION);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }
    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE SPORT (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "DESCRIPTION TEXT, "
                    + "CATEGORY TEXT);");
            for(int i =0 ; i<SaleActivity.q.size();i++) {
                QBCustomObject qb = SaleActivity.q.get(i);
                    insertFootball(db, qb.getString("Name"), qb.getString("description"), qb.getString("category"));
            }
        }
        if (oldVersion < 2) {
            db.execSQL("ALTER TABLE SPORT ADD COLUMN FAVORITE NUMERIC;");
        }
    }
    private static void insertFootball(SQLiteDatabase db, String name,
                                       String description, String category ) {
        ContentValues bootsValues = new ContentValues();
        bootsValues.put("NAME", name);
        bootsValues.put("DESCRIPTION", description);
        bootsValues.put("CATEGORY", category);
        db.insert("SPORT", null, bootsValues);
    }

}
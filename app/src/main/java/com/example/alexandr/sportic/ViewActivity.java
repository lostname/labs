package com.example.alexandr.sportic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.Consts;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.FileHelper;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.io.File;
import java.io.InputStream;

public class ViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        QBCustomObject view= SaleActivity.q.get(0);
        TextView name = (TextView)findViewById(R.id.name);
        TextView category = (TextView)findViewById(R.id.category);
        TextView description = (TextView)findViewById(R.id.description);
        String n = view.get("name").toString();
        String c = view.get("category").toString();
        String d = view.get("description").toString();
        name.setText(n);
        description.setText(d);
        category.setText(c);



    }

}

package com.example.alexandr.sportic;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

public class SaleActivity extends AppCompatActivity  {
    public interface Contract {
        String NAME = "name";
        String DESCRIPTION = "description";
        String category = "category";
        String RATING = "rating";

    }



    public static ArrayList<QBCustomObject> q=new ArrayList<QBCustomObject>();
    public  static ArrayList<String> list= new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);

        if(isOnline.isOnline(SaleActivity.this)==true){
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, list);
            ListView ss = (ListView)findViewById(R.id.list);
            ss.setAdapter(adapter);
            AdapterView.OnItemClickListener itemClickListener =
                    new AdapterView.OnItemClickListener(){
                        public void onItemClick(AdapterView<?> listView,
                                                View v,
                                                int position,
                                                long id) {
                            if (position == 0) {
                                Intent intent = new Intent(SaleActivity.this,
                                        ViewActivity.class);
                                startActivity(intent);
                            }
                        }
                    };
            ss.setOnItemClickListener(itemClickListener);

            QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
            requestBuilder.or("category", "fantasy");
            QBCustomObjects.getObjects("Movie", requestBuilder, new QBEntityCallback<ArrayList<QBCustomObject>>() {
                @Override
                public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
                    int c = customObjects.size();
                    TextView textView = (TextView) findViewById(R.id.description);

                    q = customObjects;
                    for (int i = 0; i < customObjects.size(); i++) {
                        QBCustomObject qb = customObjects.get(i);
                        String s = qb.getString("name");

                        list.add(s);
                    }
                    String l = Integer.toString(list.size());

                    textView.setText(l);

                    String a[] = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                a[i] = list.get(0);
            }


        }

        @Override
        public void onError(QBResponseException errors) {
            String a = "HEllo";
            TextView t = (TextView) findViewById(R.id.description);
            t.setText(a);
        }

    });

}else{
            try {
                SQLiteOpenHelper starbuzzDatabaseHelper = new FeedReaderDbHelper(this);
                SQLiteDatabase db = starbuzzDatabaseHelper.getReadableDatabase();
                Cursor cursor = db.query("SPORT",
                        new String[]{"NAME", "DESCRIPTION", "CATEGORY"},
                        "_id = ?",
                   null,
                        null, null, null);
                if (cursor.moveToFirst()) {
                    //Получение данных напитка из курсора
                    String nameText = cursor.getString(0);
                    String descriptionText = cursor.getString(1);
                    int photoId = cursor.getInt(2);
                    //Заполнение названия напитка
                    TextView name = (TextView) findViewById(R.id.name);
                    name.setText(nameText);
                    //Заполнение описания напитка
                    TextView description = (TextView) findViewById(R.id.description);
                    description.setText(descriptionText);
                    //Заполнение изображения напитка
                    TextView category = (TextView) findViewById(R.id.category);
                    category.setText(descriptionText);

                }
                cursor.close();
                db.close();
            } catch (SQLiteException e) {
                Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
}


    }


}

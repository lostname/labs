package com.example.alexandr.sportic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBSettings;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements Const{

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//
        QBSettings.getInstance().init(getApplicationContext(), APP_ID, AUTH_KEY, AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      final  QBCustomObject newRecord = new QBCustomObject();
        newRecord.putString("name", "Star Wars");
        newRecord.putString("description", "Star Wars is an American epic space opera franchise consisting of a film series created by George Lucas.");
        newRecord.putString("category", "fantasy");
        newRecord.setClassName("Movie");
        newRecord.putString("name", "Adidas");
        newRecord.putString("description", "goodooG.");
        newRecord.putString("category", "hockey");
        newRecord.setClassName("Movie");

        QBAuth.createSession(new QBUser("sashki12", "sashki12"), new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                QBCustomObjects.createObject(newRecord, new QBEntityCallback<QBCustomObject>() {
                    @Override
                    public void onSuccess(QBCustomObject createdObject, Bundle params) {

                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        String a ="HEllo";
                        TextView t = (TextView)findViewById(R.id.error);
                        t.setText(a);
                    }
                });
                // success
            }

            @Override
            public void onError(QBResponseException error) {
             String a ="HEllo";
                TextView t = (TextView)findViewById(R.id.error);
                t.setText(a);
            }
        });
        AdapterView.OnItemClickListener itemClickListener =
                new AdapterView.OnItemClickListener(){
                    public void onItemClick(AdapterView<?> listView,
                                            View v,
                                            int position,
                                            long id) {
                        if (position == 1) {
                            Intent intent = new Intent(MainActivity.this,
                                    SaleActivity.class);
                            startActivity(intent);
                        }
                    }
                };
        //Добавить слушателя к списковому представлению
        ListView listView = (ListView) findViewById(R.id.list_options);
        listView.setOnItemClickListener(itemClickListener);
    }


}

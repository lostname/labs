package com.example.alexandr.sportic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.quickblox.content.QBContent;
import com.quickblox.core.Consts;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;

import java.io.InputStream;

public class Image extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        final int fileId = 3607775;

        QBContent.downloadFileById(fileId, new QBEntityCallback<InputStream>() {
            @Override
            public void onSuccess(final InputStream inputStream, Bundle params) {
                long length = params.getLong(Consts.CONTENT_LENGTH_TAG);


                // use inputStream to download a file
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int progress) {

            }
        });
    }
}
